This LivWell marijuana dispensary, itself, is one of LivWell�s largest, with spacious, well-lit budrooms built to accommodate a large volume of customers coupled with a friendly and inviting atmosphere to ensure a comfortable and pleasant visit.

Address: 2647 8th Ave, Garden City, CO 80631, USA

Phone: 970-616-6007

Website: https://www.livwell.com/garden-city-marijuana-dispensary/